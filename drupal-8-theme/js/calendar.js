/**
 * @file
 * Fullcalendar View plugin JavaScript file.
 */

(function ($, Drupal) {
  Drupal.behaviors.fullcalendarView = {


    attach: function (context, settings) {
      var _attach = this;
      $('.calendar--entity').once('processed-calendar').each(function(index) {

        var _this = this;
        // Get the data-view-display so we can get the config for this calendar
        var view_display = $(this).data("view-display");
        $calendar_config = drupalSettings.calendar[view_display];
        _this.calendar_config = $calendar_config;
        var calendarConfig = {
          header: {
            left: '',
            center: 'prev, title, next',
            right: $calendar_config.rightButtons
          },

          views: ($calendar_config.view ) ? $calendar_config.view : {
              month: {
                columnHeaderFormat:'dddd',
                titleFormat: 'MMMM YYYY',
                eventLimit: false
              },
              listMonth:{
                columnHeaderFormat:'D',
                rowFormat:'D',
                eventLimit: false
              },
              'default':{
                columnHeaderFormat:'D',
                titleFormat: 'MMMM D YYYY',
                eventLimit: false
              }
            },

          defaultDate: $calendar_config.defaultDate,
          locale: $calendar_config.defaultLang,
          navLinks: ($calendar_config.navLinks == 0) ? false : true,
          editable: false,
          eventLimit: true, // Allow "more" link when too many events.
          events: $calendar_config.fullCalendarView,
          eventOverlap: ($calendar_config.alloweventOverlap == 0) ? false : true,
          eventRender: function (event, $el) {

            // Popup tooltip.
            if (event.description && $calendar_config.add_qtip) {
              if ($el.fullCalendarTooltip !== "undefined") {
                $el.qtip({
                  content: {
                    text: event.description,
                    title: event.title
                  },
                  position: {
                    my: 'bottom center',  // Position my bottom center...
                    at: 'top center' // at the top center of...
                  },
                  style: {
                    classes: 'rdn-qtips qtip qtip-tipsy qtip-shadow'
                  }
                });
              }
            }
            // Recurring event.
            if (event.ranges) {
              return (event.ranges.filter(function (range) {
                  if (event.dom) {
                    var isTheDay = false;
                    var length = event.dom.length;
                    for (var i = 0; i < length; i++) {
                      if (event.dom[i] == event.start.format('D')) {
                        isTheDay = true;
                        break;
                      }
                    }
                    if (!isTheDay) {
                      return false;
                    }
                  }
                  // Test event against all the ranges.
                  if (range.end) {
                    return (event.start.isBefore(moment.utc(range.end, 'YYYY-MM-DD')) &&
                    event.end.isAfter(moment.utc(range.start, 'YYYY-MM-DD')));
                  }
                  else {
                    return event.start.isAfter(moment.utc(range.start, 'YYYY-MM-DD'));
                  }
                }).length) > 0; // If it isn't in one of the ranges, don't render it (by returning false)
            }
          },

          eventResize: function (event, delta, revertFunc) {
            // As designed, the end date is inclusive for all day event,
            // which is not what we want. So we need one day subtract.

            if (event.allDay) {
              event.end.subtract(1, 'days');
            }
            if (drupalSettings[index].updateConfirm == 1 && !confirm(event.title + " end is now " + event.end.format() + ". Do you want to save the change?")) {
              revertFunc();
            }
          },
          // Event on mouse hover a event
          eventMouseover: function(event, jsEvent, view ) {

            if (_this.calendar_config.join_qtip) {
              // Change the tooltip so we can have all the events description on one
              $(this).once('processed--event-grid').each(function() {
                var tooltipTitle = '<ul>';
                var currentTarget = $(jsEvent.currentTarget);
                var cell_date = moment.utc(event.start._i);
                var events = $(_this).fullCalendar("clientEvents", function(event) { return event.start.startOf("day").isSame(cell_date); });
                if (events.length > 0){
                  $.each(events, function( index, value ) {
                    tooltipTitle = tooltipTitle +'<li>' + value.title + '</li>';
                  });
                  tooltipTitle = tooltipTitle + '</ul>';
                  if (currentTarget.data('qtip')){
                    currentTarget.data('qtip').options.content.text = tooltipTitle;
                    currentTarget.data('qtip').options.content.title = '';
                  }
                }
              });
            }
          },
          // Event after finished rendering
          eventAfterAllRender: function(view, event, element) {
            // Show calendar form

            if ($(".calendar-date-form.hidden")){
              var calendar__form = $(".calendar-date-form");
              var cal_form = calendar__form.clone(true, true);
              calendar__form.remove();
            }

            $(document).ready(function($) {
              $(".calendar-grid-page .fc-toolbar").once('processed-date').each(function( index) {
                cal_form.insertAfter(".calendar-grid-page .fc-header-toolbar");
                $(".calendar-date-form").removeClass('hidden');
                $(".calendar-date-form select").chosen();
              });
            });

            // Change the date format on list view
            $('.fc-list-table .fc-list-heading ').once('processed-date').each(function( index) {
              var userDate = $(this).data("date");
              var date_string = moment(userDate, "YYYY-MM-DD").format("D");
              $(this).find('.fc-list-heading-main').html(date_string);

            });

            // Add qtip tooltip on calendar render
            if (_this.calendar_config.join_qtip) {

              $(_this).find('a.fc-event').once('processed-date').each(function( index) {
                if (!$(this).data('qtip')){
                  $(this).qtip({
                    content: {
                      text: '',
                      title: ''
                    },
                    position: {
                      my: 'bottom center',  // Position my bottom center...
                      at: 'top center' // at the top center of...
                    },
                    style: {
                      classes: 'rdn-qtips qtip qtip-tipsy qtip-shadow'
                    }
                  });
                }
              });
            }
          },
          // Event on click on the days numbers
          dayClick: function(date, jsEvent, view) {
          }
        };

        // Build up the calendar with the config
        $(this).fullCalendar(calendarConfig);

        if ($calendar_config.breakpoint) {
          _attach.windowResizeCalendarView(context, $calendar_config);
        }
      })
    },

    windowResizeCalendarView: function(context, calendarConfig){
      var toggleList = function (_context) {
        // Check that are hitting the breakpoint.
        if($(window).width() <= calendarConfig.breakpoint) {
          // Ensure we are in list view.
          $('.calendar--entity .fc-listMonth-button').trigger("click");
        } else {
          $('.calendar--entity .fc-month-button').trigger("click");
        }
      };
      $(window).once('calendar-resize').resize(function () {
        toggleList(context);
      });
      toggleList(context);
    }
  };
})(jQuery, Drupal);
