(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.CustomTweaks = {
    attach: function (context) {

    }
  };

  Drupal.behaviors.MobileMenu = {
    attach: function (context) {
      setTimeout(function () { // wait till mm-navbar is initialized else where           
        var API = $("#off-canvas").data( "mmenu" );

        $('.mm-navbar', context).each(function () { // add close for each navbar item
          $(this)
            .append("<a href='#' class='close mobile-menu-mm-btn-close' aria-label='Close mobile menu' aria-controls='mobile-menu-item-control' id='mobile-menu-close' role='button'></a>")
            .on('click', '.close', function(event) {
              event.preventDefault();
              API.close();
            });
        });
      });

      $(window).once('window-mobile-process').resize(function() {
        if ($(window).width() >= 740) {
          $('.mm-navbar .close:visible').first().click(); // click first visible close button
        }
      });
    }
  };

  Drupal.behaviors.placeHolderSearch = {
    attach: function (context) {
      $(document).ready(function($) {
        var searchType = $(".content-search-list .views-infinite-scroll-content-wrapper .views-row > article");
        var searchField = $(".content-search-list .views-exposed-form .form-text");

        if (searchType.hasClass("node--type-exhibition")) {
          searchField.attr("placeholder", "Search Exhibitions");
        } else if (searchType.hasClass("node--type-event")) {
          searchField.attr("placeholder", "Pick a Date");
        } else if (searchType.hasClass("node--type-lesson-plan")) {
          searchField.attr("placeholder", "Search Lesson Plans");
        } else if (searchType.hasClass("node--type-blog")) {
          searchField.attr("placeholder", "Search Blogs");
        }

        $(".page--search .js-form-type-textfield .form-text").attr("placeholder", "Keyword...");
      });
    }
  };

  Drupal.behaviors.selectboxInputs = {
    attach: function (context) {
      $(document).ready(function($) {
        var searchField = $(".paragraph--type--content-search-list .chosen-search input");
        $.each( searchField, function( key, item ) {
          $(item).attr('data-bef-auto-submit-exclude',true);
        });
      });
    }
  };

  Drupal.behaviors.heroBannerVideo = {
    attach: function (context) {
      $(document).ready(function($) {
        var bannerVideo = $(".paragraph--type--hero-banner-video video");
        //play video
        $.each( bannerVideo, function( key, item ) {
          $(item).trigger( $(item).prop('paused') ? 'play' : 'pause');
        });
      });
    }
  };

  Drupal.behaviors.cloneAccountBlocks = {
    attach: function (context) {
      setTimeout(function () {
        $('.mm-panels', context).each(function () {
          var $ucWrapper = $('<div>', {id: 'mobile-u-c-wrapper', class: 'nav-mobile-custom-wrapper'}).insertBefore($('.mm-navbar:first', this)), // clone only into the first/top navbar item
            $cWrapper = $('<div>', {id: 'mobile-c-wrapper', class: 'nav-mobile-custom-wrapper'}).appendTo($('#mm-1:first', this));
          $( ".logged-user .nav-user-menu").clone(true, true).appendTo($ucWrapper);
          $( ".login-user .nav-user-menu").clone(true, true).appendTo($cWrapper);
        });  
      });
    }
  };

  Drupal.behaviors.sisuHeader = {
    attach: function (context) {

      //user accoount navbar behavior
      var userNavBtn = $(".nav-main-user .user-nav-link");
      var userNavCardForm = $(".nav-main-user .menu-item .menu-dropdown ");
      var userNavCard = $(".nav-main-user .menu-item");
      var pageContainer = $("#container");
      $(userNavBtn).click(function () {
        if(userNavCard.hasClass('open')){
          $(userNavCard).removeClass('open');
          $(pageContainer).removeClass('open');
          $(userNavBtn).attr('aria-expanded', 'false');
          $(userNavCardForm).attr('aria-hidden', 'true');
        }else{
          $(userNavCard).addClass('open');
          $(pageContainer).addClass('open');
          $(userNavBtn).attr('aria-expanded', 'true');
          $(userNavCardForm).attr('aria-hidden', 'false');
        }

        $(userNavCard).css('display', 'none');
        $(userNavCard).css('display', 'block');
      });

      $(pageContainer).click(function () {
        $(userNavCard).removeClass('open');
        $(pageContainer).removeClass('open');
        $(userNavBtn).attr('aria-expanded', 'false');
        $(userNavCardForm).attr('aria-hidden', 'true');
      })
    }
  };


  Drupal.behaviors.headerAccessibility = {
    attach: function (context) {
      $('.nav-main:not(.nav-main-user) .menu-level-0 > .menu-item').once('main-nav-level-0').each(function() {

        // Desktop menu

        // On focus on level one menu item
        $(this).find('> .nav-link').focusin(function (e) {
          $('.nav-main .menu-level-0 > .menu-item > .menu-dropdown').removeClass('visible').attr('aria-hidden', 'true');
          $('.nav-main .menu-level-0 > .menu-item').attr('aria-expanded', 'false');
          e.stopPropagation();
          e.preventDefault();
          e.stopImmediatePropagation();
        });

        // On focus out of a item, hide the dropdown
        this.addEventListener('focusout', function(event) {
          if (this.contains(event.relatedTarget)) {
            // fallback
            return;
          }
          $('.nav-main .menu-level-0 > .menu-item > .menu-dropdown').removeClass('visible').attr('aria-hidden', 'true');
          $('.nav-main .menu-level-0 > .menu-item').attr('aria-expanded', 'false');
        });

        // On mouse hover event for mouse users, change aria dropdown attr
        this.addEventListener('mouseover', function(event) {
          if (this.contains(event.relatedTarget)) {
            // fallback
            return;
          }
          $(this).find(' > .menu-dropdown').attr('aria-hidden', 'false');
          $(this).attr('aria-expanded', 'true');
        });

        // On mouse leave event for mouse users, change aria dropdown attr
        this.addEventListener('mouseleave', function(event) {
          if (this.contains(event.relatedTarget)) {
            // fallback
            return;
          }
          $(this).find(' > .menu-dropdown').attr('aria-hidden', 'true');
          $(this).attr('aria-expanded', 'false');
        });

        // For keyboard users, open the dropdown container when space bar press and avoid link redirect
        $(this).keypress(function(e) {
          // avoid main menu items to be triggered with a keypress apart from the ENTER key
          $(this).find(' > .menu-dropdown').addClass('visible').attr('aria-hidden', 'false');
          $(this).attr('aria-expanded', 'true');
          e.stopPropagation();
          e.preventDefault();
          e.stopImmediatePropagation();
        });
      });

      // On header focus out, all the dropdown will collapse
      $('.header').once('main-nav-level-0').focusout(function () {
        this.addEventListener('focusout', function(event) {
          if (this.contains(event.relatedTarget)) {
            // fallback
            return;
          }
          $('.nav-main .menu-level-0 > .menu-item > .menu-dropdown').removeClass('visible').attr('aria-hidden', 'true');
          $('.nav-main .menu-level-0 > .menu-item').attr('aria-expanded', 'false');
        });
      });

      // Mobile menu

      // Mobile hamburger-toggle menu button arials update
      $('.responsive-menu-toggle-icon').once('responsive-menu-toggle').each(function() {
        $('.mm-menu_offcanvas').attr('aria-hidden', 'true');
        $(this).click(function () {
          if ($(this).hasClass('open')){
            $(this).removeClass('open').attr('aria-expanded', 'false');
            $('.mm-menu_offcanvas').attr('aria-hidden', 'true');
          } else {
            $(this).addClass('open').attr('aria-expanded', 'true');
            $('.mm-menu_offcanvas').attr('aria-hidden', 'false');
          }
        });
      });

      // Need document.ready since mobile menu is builded up with js and not ready on attach
      $(document).ready(function($) {
        // Wrap mobile navigation menu in panels
        $('.mm-menu .mm-panels').once('movile-menu-nav').each(function () {
          $(this).wrapAll("<nav role='navigation' aria-labelledby='mobile-nav'></nav>");
        });

        // Mobile menu next buttons
        $('.mm-btn_next').once('responsive-menu-wrap').each(function () {
          var menuItemTitle = $(this).parent().find('a.nav-link').html();
          // wrap in a button and set attr
          $(this).attr({
            'class':          $(this).attr("class") + ' ' + 'mobile-menu-mm-btn-next',
            'aria-controls' : 'mobile-menu-item-control',
            'id':             'mobile-menu-item-btn',
            'role':           'button',
            'aria-label':     String('Open '+menuItemTitle+' submenu')
          });
          // On click set focus on the open panel
          $(this).click(function () {
            setTimeout(function(){
              $('.mm-panel_opened .mobile-menu-mm-btn-prev').focus();
            }, 100);
          });
        });

        //Mobile panels wrap menu on nav
        $('.mm-navbar .mm-btn_prev').once('mobile-back-bts').each(function () {
          var menuItemTitle = $(this).parent().find('.mm-navbar__title').html();
          $(this).attr({
            'class':          $(this).attr("class") + ' ' + 'mobile-menu-mm-btn-prev',
            'aria-controls':  'mobile-menu-item-control',
            'id':             'mobile-menu-back-btn',
            'aria-label':     'Back to '+ menuItemTitle+' menu item',
            'role':           'button'
          });
        })
      });
    }
  };

  Drupal.behaviors.faqCollapse = {
    attach: function (context) {

      $(".faq-checkbox + .faq-collapse-toggle").once('.faq-collapse').each(function() {
          $(this).click(function() {
            var collapsible = $(this).parent().find(".faq-collapse");

            // Accesiblity
            $(this).attr('aria-expanded', 'true');
            $(collapsible).attr('aria-hidden', 'false');

            $(collapsible).addClass("show");
            $(this).css("display", "none");
            var collapsibleMaxH = 0;

            // Get visible childrens height
            $(collapsible).children().each(function(){
              collapsibleMaxH = collapsibleMaxH + $(this).outerHeight(true);
            });

            $(collapsible).css('max-height', collapsibleMaxH);
            setTimeout(function(){
              $(collapsible).css('max-height', '100%');
            }, 500); //0.5 style transition
        });
      });

      $('.node--type-faq .feedback-message-container').once('feed-process').each(function() {
        var _this = this;
        $(this).find('a').click(function () {
          $(_this).attr('id', 'feedback-message');
        });
      })
    }
  };

  Drupal.behaviors.lightGallery = {
    attach: function (context) {
      $(document).ready(function($) {
        //function used to wrap the images
        function wrap(el, wrapper) {
          el.parentNode.insertBefore(wrapper, el);
          wrapper.appendChild(el);
        }
        var blog_container = document.querySelector('.node--type-blog');
        var blog_image = document.querySelectorAll('.node--type-blog img');
        var lp_container = document.querySelector('.node--type-lesson-plan');
        var lp_image = document.querySelectorAll('.node--type-lesson-plan img');

        if (blog_image && blog_container) {
          blog_container.id = 'lightgallery';
          blog_image.forEach(function(element) {
            wrap(element, document.createElement('div'));
            element.parentNode.dataset.src = element.src;
            element.parentNode.classList.add('gallery--img-container');
            // element.parentNode.thumb = element.src;

            // search for the image caption, not all the images with caption have the same layout or class, so we search and attach it to the data-sub-html of the image wrapper
            element.caption = element.parentNode.parentNode.querySelectorAll('[class *="caption"]');
            if (element.caption != null) element.caption = element.parentNode.parentNode.querySelectorAll('[class *="caption"]');
            if (element.caption != null) element.caption = element.parentNode.parentNode.parentNode.querySelectorAll('[class *="caption"]');
            if (element.caption != null) element.caption = element.parentNode.parentNode.parentNode.parentNode.querySelectorAll('[class *="caption"]');
            if (element.caption[0] && element.caption[0].children.length <= 0){
              element.parentNode.setAttribute('data-sub-html', element.caption[0].innerHTML);
            } else if(element.caption[0] && element.caption[0].children.length > 0) {
              if(element.caption[0].children[0].innerHTML) element.parentNode.setAttribute('data-sub-html', element.caption[0].children[0].innerHTML );
            }
          });

          //initiate the lightgallery and set the options
          lightGallery(document.getElementById('lightgallery'), {
            selector: '.gallery--img-container', // class of each gallery item wrapper, container
            pullCaptionUp: true, // pull caption block
            download: false, // remove download option
            thumbnail: false, // remove bottom thumbanil bar
            ddClass: 'museum--gallery', // custom class for the gallery
            closable: false, // avoid to close gallery on the gallery dimmer
            hideBarsDelay: 1000000 // delay for hiding gallery controls in ms
          });
        }

        if (lp_image && lp_container) {
          lp_container.id = 'lightgallery';
          lp_image.forEach(function(element) {
            wrap(element, document.createElement('div'));
            element.parentNode.dataset.src = element.src;
            element.parentNode.classList.add('gallery--img-container');
            // element.parentNode.thumb = element.src;

            // search for the image caption, not all the images with caption have the same layout or class, so we search and attach it to the data-sub-html of the image wrapper
            element.caption = element.parentNode.parentNode.querySelectorAll('[class *="caption"]');
            if (element.caption != null) element.caption = element.parentNode.parentNode.querySelectorAll('[class *="caption"]');
            if (element.caption != null) element.caption = element.parentNode.parentNode.parentNode.querySelectorAll('[class *="caption"]');
            if (element.caption != null) element.caption = element.parentNode.parentNode.parentNode.parentNode.querySelectorAll('[class *="caption"]');
            if (element.caption[0] && element.caption[0].children.length <= 0){
              element.parentNode.setAttribute('data-sub-html', element.caption[0].innerHTML);
            } else if(element.caption[0] && element.caption[0].children.length > 0) {
              if(element.caption[0].children[0].innerHTML) element.parentNode.setAttribute('data-sub-html', element.caption[0].children[0].innerHTML );
            }
          });

          //initiate the lightgallery and set the options
          lightGallery(document.getElementById('lightgallery'), {
            selector: '.gallery--img-container', // class of each gallery item wrapper, container
            pullCaptionUp: true, // pull caption block
            download: false, // remove download option
            thumbnail: false, // remove bottom thumbanil bar
            ddClass: 'museum--gallery', // custom class for the gallery
            closable: false, // avoid to close gallery on the gallery dimmer
            hideBarsDelay: 1000000 // delay for hiding gallery controls in ms
          });
        }


        // attach event that fire just before starting opening the gallery
        var _lgallery = document.getElementById('lightgallery');
        if(_lgallery){
          var _lg_container;

          _lgallery.addEventListener('onAfterOpen', function(e){
          // get gallery block
          _lg_container = document.querySelector('.lg-outer .lg');
          // move controls inside of the toolbar
          var _info = document.createElement('span');
          _info.classList.add('lg-info');
          _lg_container.controls = _lg_container.querySelector('.lg .lg-actions');
          _lg_container.toolbar = _lg_container.querySelector('.lg .lg-toolbar');
          _lg_container.toolbar.appendChild(_lg_container.controls);
          _lg_container.toolbar.appendChild(_info);

            _info.addEventListener('click', function(e){
              var  _caption = _lg_container.querySelector('.lg-sub-html');
              var  _img_wrap = _lg_container.querySelector('.lg-current .lg-img-wrap');
              var  _info_triger = _lg_container.querySelector('.lg-info');
              if(_caption && !(_caption.classList.contains('lg-empty-html'))) {
                _caption = (_caption.classList.contains('active')) ? _caption.classList.remove('active') : _caption.classList.add('active');
                _img_wrap = (_img_wrap.classList.contains('active')) ? _img_wrap.classList.remove('active') : _img_wrap.classList.add('active');
                _info_triger = (_info_triger.classList.contains('active')) ? _info_triger.classList.remove('active') : _info_triger.classList.add('active');
              }
            }, false);

            _lgallery.addEventListener('onAfterSlide', function(event) {
              var  _caption = _lg_container.querySelector('.lg-sub-html');
              var  _img_wrap = _lg_container.querySelector('.lg-current .lg-img-wrap');
              var  _info_triger = _lg_container.querySelector('.lg-info');
              if(_info_triger.classList.contains('active') && (_caption && ! _caption.classList.contains('lg-empty-html'))){
                  _caption.classList.add('active');
                  _img_wrap.classList.add('active');
              }

              if (_caption && _caption.classList.contains('lg-empty-html')) {
                _caption.classList.remove('active');
                _img_wrap.classList.remove('active');
              }

              if (! _info_triger.classList.contains('active')) {
                _caption.classList.remove('active');
                _img_wrap.classList.remove('active');
              }

            }, false);
          }, false);
        }
      });
    }
  };

  Drupal.behaviors.Notifications = {
    attach: function (context, settings) {
      var getClosedNotifications = function () {
        var data = localStorage.getItem('closed-notifications');
        return JSON.parse(data) || [];
      };
      $('.notification-bar', context).each(function () {
        var $notification = $(this);
        var id = $notification.data('id');

        if (!getClosedNotifications().includes(id)) { // not closed yet, show it and handle the click
          $notification.addClass('visible').find('.close-btn').on('click', function () {
            var closed = getClosedNotifications();

            closed.push(id);
            localStorage.setItem('closed-notifications', JSON.stringify(closed));
            $notification.removeClass('visible');
          });
        }
      });
  }};

  Drupal.behaviors.Table = {
    attach: function (context) {
      $(document).ready(function($) {
        var table_wrapper = document.querySelectorAll('.ticket_table_wrapper');
        if (table_wrapper) {
          table_wrapper.forEach(function(element) {
            if(element.children.length <= 0 ){
              element.remove();
            } else {
              columnSlider(element);
            }
          });
        }
      });

      function columnSlider(table_w) {
        //triggers for slider
        table_w._left = table_w.querySelector('.tickets--left-trigger');
        table_w. _right = table_w.querySelector('.tickets--right-trigger');
        if (table_w._left){
          table_w._left.addEventListener("click", function(){ slideC('left', table_w)});
        }
        if (table_w._right){
          table_w._right.addEventListener("click", function(){ slideC('right', table_w)});
        }
      }

      function slideC(direction, table_w){
        //get active items
        table_w._active = table_w.querySelectorAll('.active');
        if(direction == 'right' && table_w._active){
          table_w._active.forEach(function(table_cell) {
            if(table_cell.nextElementSibling){
              table_cell.classList.remove('active');
              table_cell.nextElementSibling.classList.add('active');
            }
          });
        }

        if(direction == 'left' && table_w._active){
          table_w._active.forEach(function(table_cell) {
            if(table_cell.previousElementSibling){
              table_cell.classList.remove('active');
              table_cell.previousElementSibling.classList.add('active');
            }
          });
        }
      }
    }
  };

  Drupal.behaviors.accordionItem = {
    attach: function (context) {
      $(document).ready(function($) {
        var accordionItem = $(".paragraph--type--accordion-item .accordion-header");

        accordionItem.on('click', function(e) {
          e.preventDefault();
          if($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this).find("button").attr("aria-expanded", "false");
          } else {
            $(this).addClass("active");
            $(this).find("button").attr("aria-expanded", "true");
          }

          $(this).siblings(".accordion-content").slideToggle("fast");
        });
      });
    }
  };

  /*
   * Form collapsible details
   */

  Drupal.behaviors.formDetailsCustomCollapsible = {
    attach: function (context) {
      collapsibleCustom();

      $(document).ajaxStop(function() {
        collapsibleCustom();
      });

      function collapsibleCustom(action) {
        // Detail form custom template collpse
        $('.form--details-custom').once('.materalize').each(function() {
          if ($(this).find('.custom--details-wrapper').attr('open') == 'open'){
            $(this).find('.custom--details-trigger').trigger('click');
          }
          $(this).find('.custom--details-wrapper').removeAttr('open');

        });

        // Add evet listener for Detail form custom template trigger
        $('.form--details-custom .collapsible').once('.materalize').each(function() {
          $(this, context).click( function () {
            $(this).toggleClass("active");
            var content = $(this.parentNode).find('.custom--details-wrapper');
            $(content).toggle( "clip");
          });
        });

      }
    }
  };

  Drupal.materializeInputs = function(element) {
    var $element = $(element);

    // Remove any placeholder from the inputs
    $element.find('input').removeAttr('placeholder');
    $element.find('label').removeClass('visually-hidden');
    $element.find('textarea').removeAttr('placeholder');

    // On inputs with value in them, set them with class
    $.each($element.find('input'), function(key, item) {
      if ($(item).val().length > 0) {
        $(item).next().addClass('active');
        $(item).addClass('active');
      }
    });

    // Add event listener for the focus in and out for the inputs
    $element.find('input').focusin(function () {
      $(this).next().addClass('active');
      $(this).addClass('active');
    });

    $element.find('input').focusout(function () {
      if($(this).val().length > 0) {
        $(this).next().addClass('active');
        $(this).addClass('active');
      } else {
        $(this).next().removeClass('active');
        $(this).removeClass('active');
      }
    });
  }

  Drupal.behaviors.materializeInputs = {
    attach: function (context) {
      var forms = [
        '.checkout-pane .form-item',
        '.paragraph--type--webform-content .webform-submission-form .form-item',
        '.museum-link-membership-form-block .form-item',
        '.page--user--edit .main-content-block .form-item',
        '.profile-form .form-item',
        '.change-pwd-form .form-item',
        '.user-form .form-item',
        '.node-form .form-item',
        '.museumMaterializeForm .form-item'
      ];

      $(forms.join()).once('.materalize').each(function() {
        Drupal.materializeInputs(this);
      });
    }
  };

  // @TODO - Move this to own JS file when DonateForm is added.
  Drupal.behaviors.donateForm = {
    attach: function(context) {
      $('.museum-donate-form').once('custom-amount-selection').each(function() {
        var $form = $(this);
        var $custom = $form.find('input.number').attr('type', 'number').on('keyup', function(e) {
          if ($(this).val() != '') {
            // Something was entered so set this to the Custom Amount.
            $('.sku-select_or_other label', $form).trigger('click');
            $('.form-item-donations-other input', $form).focus();
          }
        });
        $('.form-radio').on('click', function () {
          $custom.val('').trigger('change');
        })
      });

      // Temp solution to move custom amount field
      $('#museum-donate-form').once('custom-amount-dom').each(function() {
        var $form = $(this),
            selectWrapper   = $('#edit-donations-select', $form),
            donationsOther  = $('.form-item-donations-other', $form);

        selectWrapper.append(donationsOther);
      });

      $('.membership-trigger', context).on('click', function (e) {
        var $form = $('.museum-membership-form'),
          sku = $(this).data('sku'),
          type = $(this).data('type');

        $form.find('select').get(0).value = sku;
        $form.find('input[value="' + type + '"]').click();
      });

      // Donate pane (on membership form) to allow undo donation and support custom amount
      $('.checkout-pane-museum-donate', context).each(function () {
        var $container = $(this),
          $custom = $container.find('[name="museum_donate[custom]"]'),
          $donation = $container.find('[name^="museum_donate[donation]"]'),
          uncheck = function () {
            this.checked = false;
            $(this).trigger('change');
          };
        $donation.on('change', function (e) {
          if (this.checked) {
            $donation.not(this).each(uncheck);
            $custom.val(null);
          }
        });
        $custom.on('change', function () {
          $donation.each(uncheck);
        });
      });

      // Disable/enable next depending on Terms and condition acceptance
      $('#edit-museum-terms-conditions-terms', context).each(function () {
        var $terms = $(this),
          $form = $terms.parents('form'),
          $checkbox = $terms.find('input'),
          $toggle = $form.find('.toggle-terms'),
          $next = $form.find('#edit-actions-next');

        $checkbox.on('change', function () {
          $next.toggleClass('disabled');
          if (this.checked) {
            $next.removeAttr('disabled');
          } else {
            $next.attr('disabled', 'disabled');
          }
        });
        $toggle.on('click', function (e) {
          e.preventDefault();
          $terms.toggleClass('hidden');
        });
        $next.attr('disabled', 'disabled').addClass('disabled');
      });

      // move fields from other panes
      $('#edit-order-summary-summary .views-field-billing-profile__target-id .field-content', context).each(function () {
        if (!$(this).text().trim() && $('#edit-review-museum-contact-information-summary-billing .field-name-address').length) {
          $(this).html($('#edit-review-museum-contact-information-summary-billing .field-name-address'));
        }
      });
      $('#edit-order-summary-summary .views-field-payment-method .field-content', context).each(function () {
        if ($('#edit-review-payment-information .field.field--name-label').length) {
          $(this).html($('#edit-review-payment-information .field.field--name-label'));
          $(this).append($('#edit-review-payment-information .field.field--name-expires'));
        }
      });
      // hide empty fields
      $('#edit-order-summary-summary,#edit-museum-order-summary-4-summary', context).find('.views-field:not(td)').each(function () {
        if (!$('.field-content', this).text().trim().length) {
          $(this).hide();
        }
      });
      // copy names from address to card if they're both on the same page.
      $('.commerce-checkout-flow', context).find('#edit-museum-recipient-order-fields-field-recipient-0-address-given-name,#edit-museum-recipient-order-fields-field-recipient-0-address-family-name').on('change', function () {
        var $target = $('#edit-museum-recipient-order-fields-field-card-name-0-value');
        var val = $('#edit-museum-recipient-order-fields-field-recipient-0-address-given-name').val() + ' ' + $('#edit-museum-recipient-order-fields-field-recipient-0-address-family-name').val();

        $target.val(val).trigger('change').trigger('focusout');
      });
      if ($('.commerce-checkout-flow .order.item.annual.renewal', context).length) {
        $('.commerce-checkout-flow .renew-date', context).removeClass('hidden').before('<br>');
      }
      // move the back link to the proper place
      $('.commerce-checkout-flow #edit-actions', context).append($('.commerce-checkout-flow a.back', context).removeClass('hidden'));
      $('.field--type-address div.custom--details-trigger.collapsible', context).each(function () {
        $(this).children().remove();
        $('<div>').append($(this).text()).insertAfter($(this)).addClass('sub-heading');
        $(this).remove();
      });
      $('.checkout-pane-museum-payment-information', context).each(function () {
        var $form = $(this).parents('form'),
          $payment = $(this),
          togglePayment = function (e) {
            if (this.checked) {
              e ? $payment.slideDown() : $payment.show(); // if event trigger, animate; else, not
            } else {
              e ? $payment.slideUp() : $payment.hide();
            }
          },
          $trigger = $('#edit-museum-recurring-recurring, #edit-museum-annual-renew');

        togglePayment.call($trigger.get(0)); // hide/show on init
        $trigger.off('change', togglePayment); // avoid doubly binding on ajax (trigger is outside the context)
        $trigger.on('change', togglePayment);

        // text changes:
        $payment.find('fieldset label:last').text(Drupal.t('Add new credit card'));
        $form.find('#edit-actions-next').attr('value', Drupal.t('Save'));
      });
    }
  };

  Drupal.behaviors.SearchPageFacets = {
    attach: function (context) {
      ResetFilter();

      // Set focus on search input on page load
      $(document).ready(function($) {
        $('.page--search #edit-search-api-fulltext').focus();
      });

      $(document).ajaxStop(function() {
        ResetFilter();
        $('.page--search #edit-search-api-fulltext').focus();
      });

      function ResetFilter() {
        if ($(context).find('.page--search .block-facets-ajax').once('custom--filter-placed').length > 0) {
          var facetsItems = $('.page--search .block-facets-ajax .facet-item');
          var activeItem = $('.page--search .block-facets-ajax .facet-item a').hasClass('is-active');
          if (facetsItems[0] && !$(facetsItems[0]).hasClass('allFilter')) {
            $(facetsItems[0]).clone(true, true).insertBefore(facetsItems[0]);
            var facetsItems = $('.page--search .block-facets-ajax .facet-item');
            $(facetsItems[0]).css({"max-width": "0", "opacity": "0", 'margin': '0'});
            $(facetsItems[0]).find('a').text(Drupal.t('All'));
            if (!activeItem) $(facetsItems[0]).find('a').addClass('is-active');
            $(facetsItems[0]).find('a').attr('href', ($(facetsItems[0]).find('a').attr('href')).replace( new RegExp('(\&type)(.*)'), ''));
            setTimeout(function(){ $(facetsItems[0]).addClass('allFilter'); }, 50);
          }
        }
      }
    }
  };

  Drupal.behaviors.hashTagLinks = {
    attach: function(context) {

      if ( window.location.hash ) {
        var hash = window.location.hash,
          $hash = $(hash);

        // Check if this hash has horizontal tabs in their parents.
        if ($hash.parents('.field-group-tabs-wrapper').length == 0) {
          $hash.before('<div id="'+hash.slice(1)+'" class="hashlink"></div>');
        }
      }

      $(document).ready(function($) {
        $('html').once('hashTagProcess').each(function() {
          var offset = $(':target').offset();
          if(offset){
            var scrollto = offset.top - 100; // minus fixed header height
            $('html, body').animate({scrollTop:scrollto}, 500);
          }
        });
      });

      $('.container a[href *="#"]').once('hashTag').each(function() {
        var href = $(this).attr('href');
        if (href[0] == '#') {
          $(this).click(function (e) {
            var target = href.replace('#', '');
            if (target.length) {
              var $target = $("[id='"+ target +"']");
              if ($target.length) {
                e.preventDefault();
                $('html, body').animate({
                  scrollTop: $target.offset().top - 100
                }, 500, function () {
                });
                return false;
              }
            }
          });
        }
      });
    }
  };

  Drupal.behaviors.givingHistory = {
    attach: function(context) {
      var paid = $('.membership-giving-history .views-field-total-paid__number .field-content'),
          completed = $('.membership-giving-history .views-field-completed');

      paid.appendTo(completed);
    }
  };

  Drupal.behaviors.paymentInfoOrdering = {
    attach: function(context) {
      var stripe  = $('.checkout-pane .stripe-form'),
          payinfo = $('.checkout-pane .stripe-form + .form-wrapper');

      stripe.insertAfter(payinfo);
    }
  };

  Drupal.behaviors.videoPlayButton = {
    attach: function(context) {
      var video = $('#video-container').find('video').get(0);

      $('.c-pp').on('click', function() {
        if (video.paused) {
          $(this).removeClass('is-play');
        } else {
          $(this).addClass('is-play');
        }

        $('#video-container').find('video').get(0).paused ? $('#video-container').find('video').get(0).play() : $('#video-container').find('video').get(0).pause();

        if ($(this).hasClass('is-play')) {
          $('#videoControl').attr('aria-label', 'Play video');
        } else {
          $('#videoControl').attr('aria-label', 'Pause video');
        };
      });
    }
  };

  Drupal.behaviors.menuHoverIntent = {
    attach: function(context) {
      $('.nav-main:not(.nav-main-user) .menu-level-0 > .menu-item').hoverIntent(mousein_trigger, mouseout_trigger);

      function mousein_trigger() {
        $(this).find(' > .menu-dropdown').css({opacity: "1"}).css({visibility: "visible"});
      }

      function mouseout_trigger() {
        $(this).find(' > .menu-dropdown').css({opacity: "0"}).css({visibility: "hidden"});
      }
    }
  };

}(jQuery, Drupal, drupalSettings));
